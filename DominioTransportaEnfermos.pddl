(define (domain transporte-enfermos)

    (:predicates(en-ciudad ?x ?y)(conexion-vial ?x ?y)(enfermo-en-ciudad ?x ?y)(ambulancia-vacia ?x)(hospital-en-ciudad ?x ?y)
    )

    (:action sube-paciente
        :parameters (?Origen ?ambulancia ?paciente)
        :precondition (and(en-ciudad ?ambulancia ?Origen)(enfermo-en-ciudad ?paciente ?Origen)(ambulancia-vacia ?ambulancia))
        :effect (and(not(enfermo-en-ciudad ?paciente ?Origen))(not(ambulancia-vacia ?ambulancia)))
    )
    (:action transportar
        :parameters (?Origen ?Destino ?ambulancia)
        :precondition (and (not(en-ciudad ?ambulancia ?Destino))(conexion-vial ?Origen ?Destino)(en-ciudad ?ambulancia ?Origen))
        :effect (and (not(en-ciudad ?ambulancia ?Origen))(en-ciudad ?ambulancia ?Destino))
    )
    (:action bajar-paciente
        :parameters (?ambulancia ?Destino ?paciente ?hospital)
        :precondition (and (en-ciudad ?ambulancia ?Destino)(hospital-en-ciudad ?hospital ?Destino)(not(ambulancia-vacia ?ambulancia)))
        :effect (and (enfermo-en-ciudad ?paciente ?Destino)(ambulancia-vacia ?ambulancia))
    )
)